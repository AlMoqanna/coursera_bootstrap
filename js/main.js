$(function () {
  $('[data-toggle="tooltip"]').tooltip()
});
feather.replace();
// modify carousel speed
$('.carousel').carousel({
  interval: 3000
  });
//    for popover
  $(function () {
    $('[data-toggle="popover"]').popover()
  })
  $('.popover-dismiss').popover({
   trigger: 'focus'
 })
 // log modal events
 $('#loginmodal').on('hidden.bs.modal', function (e) {
    console.log( "Modal login se termino de ocultar");
 });
 $('#loginmodal').on('hide.bs.modal', function (e) {
     console.log( "Modal login se oculta");
 });
 $('#loginmodal').on('show.bs.modal', function (e) {
      console.log( "Modal login se muestra");
 });
 $('#loginmodal').on('shown.bs.modal', function (e) {
     console.log( "Modal login se termina de mostrar");
 });

 //newsletter modal
 $('#newsmodal').on('hidden.bs.modal', function (e) {
    console.log( "Modal newsletter se termino de ocultar");
    // reenable newsletter button
    $('#btn-newsletter').prop('disabled', false);
    $('#btn-newsletter').removeClass('btn-outline-secondary');
    $('#btn-newsletter').addClass('btn-outline-danger');
 });
 $('#newsmodal').on('hide.bs.modal', function (e) {
     console.log( "Modal newsletter se oculta");
 });
 $('#newsmodal').on('show.bs.modal', function (e) {
      console.log( "Modal newsletter se muestra");
      // disable newsletter button
      $('#btn-newsletter').removeClass('btn-outline-danger');
      $('#btn-newsletter').addClass('btn-outline-secondary');
      $('#btn-newsletter').prop('disabled', true);
 });
 $('#newsmodal').on('shown.bs.modal', function (e) {
     console.log( "Modal newsletter se termina de mostrar");
 });
