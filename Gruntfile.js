const sass = require('node-sass');

module.exports = function( grunt ) {
  require('time-grunt')(grunt);
  require('jit-grunt')(grunt,{
    useminPrepare: 'grunt-usemin'
  });
  // Configuration
  grunt.initConfig({
    sass: {
      options: {
            implementation: sass,
            sourceMap: false
      },
      dist: {
        files: [{
          expand:true,
          cwd: 'css',
          src: ['*.scss'],
          dest: 'css',
          ext: '.css'
        }]
      }
    },
    watch: {
      files: ['css/*.scss'],
      task: ['css']
    },
    browserSync: {
      dev: {
        bsFiles: {
          src: [
            'css/*.css',
            'js/*.js',
            '*.html'
          ]
        },
        options: {
          watchTask: true,
          server: {
            baseDir: './'
          }
        }
      }
    },
    imagemin: {
      dynamic: {
        files: [{
          expand: true,
          cwd: './',
          src: 'images/*.{png,jpg,jpeg,gif}',
          dest: 'dist'
        }]
      }
    },
    copy: {
      html: {
        files: [{
          expand: true,
          dot: true,
          cwd: './', //current working directory
          src: ['*.html'],
          dest: 'dist'
        }
      ]},
      fonts: {
        files: [{
          expand: true,
          dot: true,
          cwd: 'node_modules/open-iconic/font', //current working directory
          src: ['fonts/*.*'],
          dest: 'dist'
        }
      ]
      }

    },
    clean: {
      build: {
        src: ['dist/']
      }
    },
    cssmin: {
      dist: {}
    },
    uglify: {
      dist: {}
    },
    filerev: {
      options: {
        algorithm: 'md5',
        length: 15
      },
      files: {
        src: ['dist/css/*.css', 'dist/js/*.js']
      }
    },
    concat: {
      options: {
        separator: ','
      },
      dist: {}
    },
    useminPrepare: {
      foo: {
        dest: 'dist',
        src: ['index.html','about.html','faq.html','contact.html','ebook-service.html','payment.html']
      },
      options: {
        flow: {
          steps: {
            css: ['cssmin'],js: ['uglify']
          },
          post: {
            css: [{
              name: 'cssmin',
              createConfig: function(context, block) {
                var generated = context.options.generated;
                generated.options = {
                  keepSpecialComments: 0,
                  rebase: false
                }
              }
            }]
          }
        }
      }
    },
    usemin: {
      html: ['dist/index.html','dist/about.html','dist/faq.html','dist/contact.html','dist/ebook-service.html','dist/payment.html'],
      options: {
        assetsDir: ['dist', 'dist/css', 'dist/js']
      }
    }

  });


  // Load plugins
  grunt.loadNpmTasks('grunt-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-browser-sync');
  grunt.loadNpmTasks('grunt-contrib-imagemin');

  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-usemin');
  grunt.loadNpmTasks('grunt-filerev');

  // Register tasks
  grunt.registerTask('css', ['sass'] );
  grunt.registerTask('img:compress', ['imagemin'] );
  grunt.registerTask('default', ['browserSync', 'watch']);

  grunt.registerTask('build', [
    'clean',//Borramos el contenido de dist
    'copy',//Copiamos los archivos html adist
    'imagemin', //Optimizamos imagenes y las copiamos a dist
    'useminPrepare', //Preparamos la configuracion de usemin
    'concat',
    'cssmin',
    'uglify',
    'filerev', //Agregamos cadena aleatoria
    'usemin' //Reemplazamos las referencias por los archivos generados por filerev
  ]
  );
}
